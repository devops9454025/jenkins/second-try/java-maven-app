def incrementVersion() {
    echo 'Incrementing version of the application...'
    sh "mvn build-helper:parse-version versions:set \
        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
        versions:commit"
    def versionMathcer = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = versionMathcer[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}


def buildJar() {
    echo 'building the application...'
    sh 'mvn clean package'
}

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t petratos88/java-maven-app:$IMAGE_NAME ."
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh "docker push petratos88/java-maven-app:$IMAGE_NAME"
    }
}

def commitVersion() {
    echo "committing the version increment"
    withCredentials([usernamePassword(credentialsId: 'gitlab', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'git config --global user.email "jenkins@example.com"'
        sh 'git config --global user.name "jenkins"'

        sh "git status"
        sh "git branch"
        sh "git config --list"

        // Construct the GitLab repository URL
        def gitlabRepoUrl = "https://${USER}:${PASS}@gitlab.com/devops9454025/jenkins/second-try/java-maven-app.git"
        sh "git remote set-url origin ${gitlabRepoUrl}"
        sh "git add ."
        sh "git commit -m \"ci: version bump to ${IMAGE_NAME}\""
        sh "git push origin HEAD:main"
    }
}

def deployApp() {
    echo 'deploying the application...'
}


return this
